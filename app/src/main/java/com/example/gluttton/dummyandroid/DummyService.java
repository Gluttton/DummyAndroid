package com.example.gluttton.dummyandroid;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;
import android.view.Gravity;
import android.util.Log;

public class DummyService extends Service {
    public DummyService() {
        Log.d("DummyService", "DummyService()");
    }

    @Override
    public void onCreate() {
        Log.d("DummyService", "onCreate()");
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("DummyService", "onBind()");
        return mBinder;
    }

    private final IDummyAidlInterface.Stub mBinder = new IDummyAidlInterface.Stub() {
        public void createToast(String message){
            Log.d("DummyService", "createToast(String message)");
            Toast toast = Toast.makeText (getApplicationContext(), message, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    };
}
