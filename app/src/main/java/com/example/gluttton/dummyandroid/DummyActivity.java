package com.example.gluttton.dummyandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.ServiceConnection;
import android.content.ComponentName;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.os.RemoteException;
import android.content.Intent;
import android.content.Context;

public class DummyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("DummyActivity", "onCreate(Bundle)");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dummy);
        Intent intent = new Intent(this, DummyService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    IDummyAidlInterface mDummyService;

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.d("DummyActivity", "onServiceConnected(ComponentName, IBinder)");
            mDummyService = IDummyAidlInterface.Stub.asInterface(service);
        }

        public void onServiceDisconnected(ComponentName className) {
            Log.d("DummyActivity", "onServiceDisconnected(ComponentName)");
            mDummyService = null;
        }
    };

    public void onClickOne(View view) {
        try {
            Log.d("DummyActivity", "onClickOne(View)");
            mDummyService.createToast("One");
        }
        catch (RemoteException e) {
            Log.d("DummyActivity", "onClickOne(View), exception");
        }
    }

    public void onClickTwo(View view) {
        try {
            Log.d("DummyActivity", "onClickTwo(View)");
            mDummyService.createToast("Two");
        }
        catch (RemoteException e) {
            Log.d("DummyActivity", "onClickTwo(View), exception");
        }
    }
}
