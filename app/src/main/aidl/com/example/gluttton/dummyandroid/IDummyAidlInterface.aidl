// IDummyAidlInterface.aidl
package com.example.gluttton.dummyandroid;

// Declare any non-default types here with import statements

interface IDummyAidlInterface {
    void createToast(String message);
}
