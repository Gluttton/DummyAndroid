LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_PRIVATE_PLATFORM_APIS := true
#LOCAL_PRIVILEGED_MODULE := true
LOCAL_CERTIFICATE  := platform
LOCAL_MODULE_TAGS  := optional
LOCAL_PACKAGE_NAME := DummyToaster
LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/app/src/main/res
LOCAL_MANIFEST_FILE:= app/src/main/AndroidManifest.xml
LOCAL_SRC_FILES    := $(call all-java-files-under, app/src/main/java) \
                      $(call all-Iaidl-files-under, app/src/main/aidl)

LOCAL_STATIC_ANDROID_LIBRARIES := \
    android-support-v4 \
    android-support-v7-appcompat \
    android-support-constraint-layout

include $(BUILD_PACKAGE)
